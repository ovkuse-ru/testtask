#!/bin/bash

if [ -d "/var/www/html/wp-admin/" ]; then
    return
fi

sudo apt-get update
sudo apt-get -y install unzip git apache2 php5 mysql-server-5.5 php5-mysql libapache2-mod-php5 php5-mcrypt php5-cli php5-curl php5-common phpmyadmin

echo "Include /etc/phpmyadmin/apache.conf" | sudo tee --append /etc/apache2/apache2.conf > /dev/null
sudo php5enmod mcrypt
sudo service apache2 restart
sudo cp /vagrant/config/config.inc.php /etc/phpmyadmin/config.inc.php


sudo chmod 0777 /var/www/
sudo rm -rf /var/www/html/


curl -L https://ru.wordpress.org/wordpress-3.3.1-ru_RU.zip > wordpress.zip
unzip wordpress.zip -d /var/www/
mv /var/www/wordpress /var/www/html/
rm wordpress.zip

cd /var/www/html/
cp /vagrant/config/wp-config.php /var/www/html/
cp /vagrant/config/screen.php /var/www/html/wp-admin/includes/screen.php
cat /vagrant/dump.sql | mysql -u root

wget https://phar.phpunit.de/phpunit.phar
chmod +x phpunit.phar
sudo mv phpunit.phar /usr/local/bin/phpunit


ln -s /vagrant/wp-question/ /var/www/html/wp-content/plugins/wp-question